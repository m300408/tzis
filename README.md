# to zarr in swift 

This small package enables writing cloud-optimized [zarr-formatted](https://zarr.readthedocs.io/en/stable/index.html) data to DKRZ's institutional cloud [swift](https://www.dkrz.de/up/systems/swift)

## Installation
```
pip install git+https://gitlab.dkrz.de/data-infrastructure-services/tzis.git#egg=tzis
```

## Usage Example

```
from tzis import tzis

token=tzis.get_token(ACCOUNT, USERNAME=USERNAME)

path_var = PATH_TO_VAR
mfs_towrite=[path_var +filename for filename in os.listdir(path_var)]

varname = pr

container = tzis.Tzis(token["OS_STORAGE_URL"],
                      token["OS_AUTH_TOKEN"],
                      container_name,
                      mf_dset=mfs_towrite,
                      varname=varname,
                      verbose=True             
)

prefix_for_object_storage=ZARR-DSET-NAME
container.open_store(prefix_for_object_storage)

container.write_to_swift(chunkdim="time",
                       target_mb=1000,
                       startchunk=0,
                       validity_check=False,
                       maxretries=3)
                      
print(container.store.url)
                       
container.write_catalog(catalogname="catalog.json",
                      pattern=None,
                      delim=".",
                      columns=None,
                      mode="a"
                     )

```

## Tutorial Notebooks

- You can find a detailed introduction [here](https://gitlab.dkrz.de/data-infrastructure-services/tzis/-/blob/master/notebooks/tzis-tutorial.ipynb).
- We provide a use case for CMIP6 data [here](https://gitlab.dkrz.de/data-infrastructure-services/tzis/-/blob/master/notebooks/use-case_climate-data-to-swift_tzis_cmip6.ipynb)

## Service

On a Openstack VM, we run a jupyter server with the extension [appmode](https://jupyter-tutorial.readthedocs.io/de/latest/web/dashboards/appmode/index.html). This allows to provide notebooks as interactive webapps.

We developed such a showcase service for **tzis** where the source data comes from a lustre-directory. When you are in the DKRZ internal network (via vpn), you can reach:
http://jupyter-appmode.cloud.dkrz.de:8888/apps/tzis-service.ipynb

You have to put in a password which you can get by request from the DM team.
You will get the output of the notebook provided in notebooks/tzis-service.ipynb.

