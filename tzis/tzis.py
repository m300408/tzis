#!/usr/bin/env python
# coding: utf-8

import fsspec
import copy
import zarr
from zarrswift import SwiftStore
from .swifthandling import *
import xarray
import os
import shutil
import math
from tqdm import tqdm
from datetime import datetime, timedelta
from functools import partial
import requests
import stat
import sys
import time
import swiftclient
import json
from . import provenance
from . import catalog

#version:
#from xarray:
try:
    from importlib.metadata import version as _version
except ImportError:
    from importlib_metadata import version as _version  # type: ignore[no-redef]

try:
    __version__ = _version("tzis")
except Exception:
    # Local copy or not installed with setuptools.
    __version__ = "999"

class Tzis:
    def __init__(
        self,
        os_url,
        os_token,
        os_container,
        os_name=None,
        mf_dset=None,
        varname=None,
        verbose=False,
        xarray_kwargs=None,
        use_fsspec=True
    ):
        self.USE_DASK = True
        if self.USE_DASK == True : 
            self.USE_DASK = self._verify_dask()
        self.DASK_CHUNK_SIZE = 70

        self.provenance = None
        self.chunkdim = None
        self.target_mb = None
        self.fsspec_store = None

        self.auth = {
            "preauthurl": os_url,
            "preauthtoken": os_token,
        }
        self.store = SwiftStore(os_container, prefix=os_name, storage_options=self.auth)
        
        if use_fsspec :
            self.os_url = get_fsspec_url(os_url, os_container)
            #fsspec requires http url for login
            os.environ["OS_STORAGE_URL"] = os_url
            os.environ["OS_AUTH_TOKEN"] = os_token
            self.fsspec_store = get_fsspec_store(self.os_url, os_token)
            if not self.fsspec_store:
                self.os_url = os_url

        self.verbose = verbose
        #
        self.varname = varname

        self.mf_dset = self.open_mf_dataset(
            mf_dset, self.varname, keep_attrs=True, xarray_kwargs=xarray_kwargs
        )

    def _verify_dask(self):
        try:
            from dask.distributed import get_client, Client, fire_and_forget
            return True
        except Exception as e:
            print(e)
            print("Could not use dask")
            return False

    def open_mf_dataset(self, mf, varname, keep_attrs=True, xarray_kwargs=None):
        # if type(mf) != list and type(mf) != str :
        #    raise ValueError("Dataset '{0}' must either be a string or a list of strings")
        if mf:
            if type(mf) == str:
                mf = [mf]
            if xarray_kwargs:
                # xarray_kwargs.update({"decode_coords":"all"})
                mf_dset = xarray.open_mfdataset(
                    mf,
                    decode_cf=True,
                    use_cftime=True,
                    data_vars="minimal",
                    coords="minimal",
                    compat="override",
                    combine_attrs="drop_conflicts",
                    **xarray_kwargs,
                )
            else:
                mf_dset = xarray.open_mfdataset(
                    mf,
                    decode_cf=True,
                    use_cftime=True,
                    data_vars="minimal",
                    coords="minimal",
                    compat="override",
                    combine_attrs="drop_conflicts",
                )  # ,
                # **dict(decode_coords="all"))

            conflict_attrs=self.get_conflict_attrs(mf,mf_dset)
            
            if self.provenance:
                if self.verbose :
                    print("You reset the original dataset and therefore restart the provenance.")
                del self.provenance
            self.provenance=provenance.Provenance()
            
            if "tracking_id" in conflict_attrs:
                self.provenance.gen_input("input_tracking_id", conflict_attrs["tracking_id"])
            else:
                self.provenance.gen_input("input_file_name", mf)
            
            mf_dset.attrs["tracking_id"]=self.provenance.tracking_id
            mf_dset.attrs.setdefault("history", "")
            mf_dset.attrs["history"]+="Converted and written to swift cloud with tzis version {0}".format(
                __version__
            )

            #if keep_attrs and conflict_attrs:
            #    for k, v in conflict_attrs.items():
            #        mf_dset.attrs[k] = v

            if not varname:
                self.varname = self._get_varname(mf_dset)
            else:
                if varname not in list(mf_dset.data_vars):
                    raise ValueError(
                        "Given variable name {0} not in dataset {1}".format(
                            varname, mf[0]
                        )
                    )
                self.varname = varname

            #            for coord in mf_dset.coords:
            #                mf_dset[coord].load()

            return mf_dset
        return None

    def get_conflict_attrs(self, mf, mf_dset):
        conflict_attrs = {}
        #try:
        maxdigits=len(str(len(mf)))
        digitstring="{0:0"+str(maxdigits)+"}"
        for fileno,dset in enumerate(mf):
            dset_attrs = xarray.open_dataset(dset).attrs
            missing_attrs = {
                k : v for k, v in dset_attrs.items() if k not in mf_dset.attrs
            }
            for k, v in missing_attrs.items():
                #attr_prefix=" File "+digitstring.format(fileno)+ ": "
                #if dset_attrs["tracking_id"]:
                #attr_prefix=" "+dset_attrs["tracking_id"] + ": "
                #conflict_attrs[k]=conflict_attrs[k] + attr_prefix + v + ","
                
                conflict_attrs.setdefault(k, [])
                conflict_attrs[k].append(v)
        #except:
        #    if self.verbose:
        #        print("Could not collect all attributes.")
        return conflict_attrs

    def _get_varname(self, mf_dset):
        return_varname = ""
        varlist = list(mf_dset.data_vars)
        for var in varlist:
            cinv = False
            for coord in ["ap", "b", "ps", "bnds"]:
                if coord in var:
                    cinv = True
                    break
            if cinv:
                continue
            return_varname = var
            break
        if not return_varname:
            raise ValueError(
                "Could not find any variable to write to swift. Please specify varname."
            )
        if self.verbose:
            print(
                "We use variable {0} in case we need to rechunk.".format(self.varname)
            )
        return return_varname

    def open_store(self, os_name):
        if type(os_name) != str :
            raise ValueError(f"{f} must be a sting")
        if self.fsspec_store :
            self.fsspec_store = fsspec.get_mapper('/'.join([self.os_url,os_name]))
        self.store.prefix = os_name
        return self.store
        
    def _get_size_of_var_timeseries(self, ds, varname):
        """returns the size of the variable `varname`of the entire dataset `ds` used for chunking.
        Dataset can be multiple files.

        Parameters
        ----------
        ds : `xarray` dataset
             `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        varname : string, user input

        Returns
        -------
        Integer
            Size of entire variable in bytes

        """
        return ds[varname].nbytes

    def _calc_chunk_bytes(self, ds, varname, chunkdim, target_mb):
        """Estimates the length of one chunk of `varname` of dataset `ds`in the chunk dimension `chunkdim`
        to match the targeted chunk size `target_mb` in the cloud.

        Parameters
        ----------
        ds : `xarray` dataset
             `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        varname : string, user input
        chunkdim : {"time"}
            - time : Only dimension that is implemented yet
        target_mb : integer, user input
            The targeted size of one chunk in the cloud. The default is 1000.

        Returns
        -------
        Integer
            Size of one chunk in bytes

        Examples
        --------

        Raises
        ------
        """
        n_bytes = self._get_size_of_var_timeseries(ds, varname)
        return math.ceil(
            len(ds[chunkdim]) / math.ceil(n_bytes / (target_mb * (2 ** 20)))
        )

    def _rechunk(self, ds, varname, chunkdim, target_mb):
        """rechunks the original dataset in order to have optimized chunk sizes.
        It sets the target chunk configuration with `xarray` function `chunk`
        and then loops over all variables in the dataset for unifying their chunks with `unify_chunks`.

        Parameters
        ----------
        ds : `xarray` dataset
             `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        varname : string, user input
        chunkdim : {"time"}
            - time : Only dimension that is implemented yet
        target_mb : integer, user input
            The targeted size of one chunk in the cloud. The default is 1000.

        Returns
        -------
        Dataset
            An `xarray` dataset with unified and rechunked chunks according.
        """
        b_is_safe_chunk=True
        orig_no_of_chunks=max([len(v) for k,v in ds[varname].chunksizes.items()])
        orig_chunk_size=ds[varname].nbytes/orig_no_of_chunks/(2 ** 20)
        if math.ceil(orig_chunk_size) < target_mb:
            print("Target_mb {0} are larger than original chunk size {1}. "
                  "Better only set target_mb to original size. "
                  "Otherwise you get dask performance issues. ".format(target_mb, orig_chunk_size))
            b_is_safe_chunk=False
            #return ds

        chunk_length = self._calc_chunk_bytes(ds, varname, chunkdim, target_mb)
                  
        chunk_rule = {chunkdim: chunk_length}

        if self.verbose:
            print(
                "Chunking into chunks of {0} {1} steps".format(chunk_length, chunkdim)
            )
        chunked_ds = ds.chunk(chunk_rule)

        for var_id in chunked_ds.variables:
            chunked_ds[var_id].unify_chunks()

        return chunked_ds, b_is_safe_chunk

    def _drop_vars_without_chunkdim(self, ds, chunkdim):
        """Drops all variables which do not depend on the chunk dimension.
        This is required because those variables cannot be written chunkwise with `write_by_region`.
        The coordinate `time_bnds` also has to be dropped
        because `xarray` does write it at first place when the dataset is initialized in the swift storage.
        
        Parameters
        ----------
        ds : `xarray` dataset
            `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        chunkdim : {"time"}
            - time : Only dimension that is implemented yet
        
        Returns
        -------
        Dataset
            An `xarray` dataset without the dropped variables.
        """
        droplist = [var for var in ds.variables if chunkdim not in ds[var].coords]
        if "time_bnds" in ds.variables:
            droplist += ["time_bnds"]
        return ds.drop_vars(droplist)

    def _sel_range_for_chunk_by_time(self, ds, starttimeindex, endtimeindex):
        """Selects a time interval spanned from `starttimeindex` to `endtimeindex` from dataset `ds`.
        The indices are controlled in a loop and chosen such that exactly one chunk is matched.

        Parameters
        ----------
        ds : `xarray` dataset
             `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        starttimeindex : Integer
            the dimension index of the interval start
        endtimeindex : Integer
            the dimension index of the interval end

        Returns
        -------
        Dataset
            A subset `xarray` dataset of the original dataset.
        """
        return ds.isel(time=slice(starttimeindex, endtimeindex))

    def _sel_range_for_chunk(self, ds, startindex, endindex, chunkdim):
        """Selects an interval spanned from `startindex` to `endindex` within the chunk dimension `chunkdim` from dataset `ds`.
        The indices are controlled in a loop and chosen such that exactly one chunk is matched.

        Parameters
        ----------
        ds : `xarray` dataset
             `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        startindex : Integer
            the dimension index of the interval start
        endindex : Integer
            the dimension index of the interval end
        chunkdim : {"time"}            - time : Only dimension that is implemented yet

        Returns
        -------
        Dataset
            A subset `xarray` dataset of the original dataset.

        Raises
        ------
        ValueError
            If the chunk dimension is not supported.
        """
        if chunkdim == "time":
            return self._sel_range_for_chunk_by_time(ds, startindex, endindex)
        else:
            raise ValueError(
                'Other chunk dimensions than "time" are not supported yet.'
            )

    def _write_chunk_by_region(self, towrite, store, chunkdim, startindex, endindex, chunk_no,
                              is_safe_chunk):
        """Selects an interval spanned from `startindex` to `endindex` within the chunk dimension `chunkdim` from dataset `ds`.
        The indices are controlled in a loop and chosen such that exactly one chunk is matched.

        Parameters
        ----------
        towrite : `xarray` dataset
             is the `xarray` dataset slice of the source dataset to be written to cloud
        chunkdim : {"time"}
            - time : Only dimension that is implemented yet
        startindex : Integer
            the dimension index of the interval start
        endindex : Integer
            the dimension index of the interval end

        Returns
        -------
        Integer
            - 0 if writing has succeeded
            - chunk_no if writing failed.
        """

        try:
            towrite.to_zarr(store=store, region={chunkdim: slice(startindex, endindex)})#,
#                           safe_chunks=is_safe_chunk)
            towrite.close()
            return 0
        except Exception as e:
            if self.verbose:
                print(e)
            return chunk_no

    def _init_dataset(self, ds, store, chunkdim, overwrite):
        if overwrite:
            try:
                prefix = self.store.prefix
                self.store.rmdir()
                self.store.prefix=prefix
            except Exception as e:
                print(e)
                if self.verbose:
                    print("Could not remove cloud.")
                pass
        try:
            #try fsspec store:
            ds.to_zarr(store, compute=False, consolidated=True, mode="w")
        except :
            if self.verbose:
                print("Could not use fsspec store to init dataset. Will use sync store.")
            #try sync store:
            ds.to_zarr(self.store, compute=False, consolidated=True, mode="w")
            pass
        chunk_independent_list = [
            var for var in ds.variables if chunkdim not in ds[var].coords
        ]
        ds[chunk_independent_list].to_zarr(store, consolidated=True, mode="r+")
        return xarray.open_zarr(
            store, consolidated=True, decode_cf=True, use_cftime=True
        )

    def _open_or_initialize_swift_dset(self, store, ds, chunkdim, overwrite=False):
        if not overwrite:
            try:
                return xarray.open_zarr(
                    store, consolidated=True, decode_cf=True, use_cftime=True
                ), False
            except:
                if self.verbose:
                    print("A new zarr dataset is created at store.")
                pass
        if self.verbose and overwrite:
            print("Cloud dataset will be overwritten.")
        try:
            return self._init_dataset(ds, store, chunkdim, overwrite), True
        except Exception as e:
            raise RuntimeError(f"Could not initialize dataset: {e}"
                              "\nTry to run `container.store.rmdir() and "
                              "\ncontainer.open_store(zarr_dset)`")

    def _check_for_overwrite(self, already, chunked_ds, varname):
        if self.verbose:
            print("Start checking if cloud must be overwritten")
            
        try:
            if already[varname].chunks != chunked_ds[varname].chunks :
                if self.verbose:
                    print(
                        f'''Chunk setting has changed from {already[varname].chunks} in cloud 
                        to {chunked_ds[varname].chunks} in your dataset.'''
                    )
                return True
        except Exception as e:
            print(e)
            print("Chunk setting has changed.")
            return True
 

        a = already.attrs
        if type(a) != dict:
            return True
        amodi = a.copy()
        c = chunked_ds.attrs.copy()
        l_differ=False
        try:
            del a["tracking_id"], a["hasProvenance"]
        except:
            l_differ=True
        del c["tracking_id"], c["hasProvenance"]
        if not a == c or l_differ :
            if self.verbose:
                print(
                    "Attributes of cloud store and source dataset differ."
                )
                return True

        for coord in already.coords:
            try:
                already[coord].load()
                if not already[coord].equals(chunked_ds[coord]):
                    if self.verbose:
                        print(
                            "Coordinate {0} is not equal to cloud store's coordinate.".format(
                                coord
                            )
                        )
                    return True
            except:
                if self.verbose:
                    print(
                        "Could not check if {0} is equal to cloud store.".format(coord)
                    )
                return True

        if self.verbose:
            print("Cloud will be appended")

        return False
    
    def _check_for_equality(self, towrite, already, chunked_ds, all_chunks,
                            chunksum, chunkdim, 
                            validity_check, chunk_no, varname):
        incloud = self._sel_range_for_chunk(
            already, chunksum - all_chunks[chunk_no], chunksum, chunkdim
        )  # .load()
        # if towrite.broadcast_equals(incloud):
        # if towrite[varname].size == incloud[varname].size :
        b_isidentical=towrite[varname].identical(incloud[varname])
        if incloud:
            incloud.close()
        if b_isidentical:
            if self.verbose:
                print("Dataset for chunk {0} are equal.".format(chunk_no + 1))
            if not validity_check:
                print("Chunk {0} is skipped.".format(chunk_no+1))
            return -1
        elif validity_check:
            print(
                "Dataset at chunk {0} from {1} are different!".format(
                    chunk_no, len(all_chunks)
                )
            )
            return chunk_no
        return 0
    
    def _write_chunk_by_region_wrapper(self, towrite, store, chunkdim,
                                       all_chunks, chunksum, chunk_no,
                                      is_safe_chunk):
        write_status = self._write_chunk_by_region(
            towrite, store, chunkdim, chunksum - all_chunks[chunk_no], chunksum,
            chunk_no, is_safe_chunk
        )
        if write_status > 0:
            return write_status
        return 0
    
    def _get_start_chunk(self, all_chunks, chunked_ds, chunksum, startchunk, chunkdim,
                        already, validity_check, varname):
        if self.verbose:
            print("Finding the start chunk can take long. You better provide the startchunk yourself")
        for chunk_no in range(0, len(all_chunks)):
            chunksum += all_chunks[chunk_no]
            if chunk_no < startchunk:
                continue
            towrite = self._sel_range_for_chunk(
                chunked_ds, chunksum - all_chunks[chunk_no], chunksum, chunkdim
            ) 
            status_equality = self._check_for_equality(towrite, already, chunked_ds, all_chunks,
                                                       chunksum, chunkdim, 
                                                       validity_check, chunk_no, varname)
            if status_equality < 0:
                continue
            elif status_equality > 0:
                return status_equality , start_chunk
            istartchunk=chunk_no
            chunksum -= all_chunks[chunk_no]
            break
        return 0 ,start_chunk


    def _reset_already_if_overwrite(self, validity_check, already, chunked_ds, store, isnew, chunkdim, varname):
        b_isnew=isnew
        if not validity_check:
            overwrite = True
            if b_isnew :
                overwrite = False
            else :
                overwrite = self._check_for_overwrite(already, chunked_ds, varname)
            if overwrite:
                already, dummy= self._open_or_initialize_swift_dset(
                    store, chunked_ds, chunkdim, overwrite=True
                )
                b_isnew = True
        return already, b_isnew


    # `write_by_region` writes chunk-wise data into an initialized dataset in swift.
    # 'Initiliazed' means that a first `to_zarr` call has been executed which writes all coordinates and metadata for the dataset into the chunk. The subsequent `to_zarr` calls performed by `write_to_region` uses this information so that it knows how chunks have to be named. If a region has already been written, it will be overwritten by write_to_region.

    def map_block_fct(self, towrite, store, chunkdim, chunkdimvalues):
        towrite_chunks=towrite[chunkdim].values
        startindex=[idx 
                    for idx,chunkdimvalue in enumerate(chunkdimvalues) 
                    if chunkdimvalue == towrite_chunks[0]
                   ]
        endindex=[idx 
                    for idx,chunkdimvalue in enumerate(chunkdimvalues) 
                    if chunkdimvalue == towrite_chunks[len(towrite_chunks)-1]
                   ]
        towrite.drop_vars(chunkdim).to_zarr(
            store=store, 
            region={chunkdim: slice(startindex[0], endindex[0]+1)},
        )#,
        return towrite
    
    def _reset_dask_chunk_sizes(self, client, target_mb):
        worker_info=client.scheduler_info()["workers"]
        avail_mem=sum(
            [worker["memory_limit"]
             for key,worker in worker_info.items()
            ])/1000/1000
        mem_per_work=avail_mem/len(worker_info)
        if mem_per_work < 10 *target_mb :
            print(f'''Note that your target_mb {target_mb} is too high for
                  available memory per worker {mem_per_work}. That leads to
                  memory leaks''')
        required_mb=self.DASK_CHUNK_SIZE * target_mb * 5
        if avail_mem < required_mb :
            if self.verbose:
                new_dask_chunk_size=int(avail_mem/(target_mb*5))-1
                print(f'''Reducing dask tasks size from {self.DASK_CHUNK_SIZE} to
                       {new_dask_chunk_size}
                       because it requires about {required_mb} MB
                       while only {avail_mem} MB are available''')
            self.DASK_CHUNK_SIZE = int(avail_mem/(target_mb*5))

    
    def _get_client_and_config(self, target_mb):
        from dask.distributed import get_client, Client
        b_islocalclient = True
        try:
            client = get_client()
            b_islocalclient = False
        except:
            client = Client()
        print(client)
        self._reset_dask_chunk_sizes(client, target_mb)
        return client, b_islocalclient
    
    def write_by_region_dask(self,
                             istartchunk,
                             all_chunks,
                             chunked_ds, 
                             chunkdim,
                             store,
                             target_mb) :
        
        client, b_islocalclient = self._get_client_and_config(target_mb)
        
        for dask_chunk in tqdm(range(istartchunk, len(all_chunks), self.DASK_CHUNK_SIZE)):
            interval_end=dask_chunk+self.DASK_CHUNK_SIZE
            if interval_end > len(all_chunks) :
                end_chunk =sum(all_chunks[0:len(all_chunks)])
                interval_end = len(all_chunks)
            else :
                end_chunk = sum(all_chunks[0:dask_chunk+self.DASK_CHUNK_SIZE])
            start_chunk=sum(all_chunks[0:dask_chunk])
            to_write=self._sel_range_for_chunk(
                chunked_ds, start_chunk, end_chunk, chunkdim
            ) 
            starttime=time.time()
            #mapped_blocks=to_write.map_blocks(self.map_block_fct,
            #                                  args=[store, chunkdim, chunked_ds[chunkdim].values],
            #                                  kwargs={},
            #                                  template=to_write)
            #result = mapped_blocks.compute()
            #del result
            self.map_block_fct(to_write, store, chunkdim, chunked_ds[chunkdim].values)        
            endtime=time.time()
            if self.verbose:
                print("Stored data with {0}mb/s".format(target_mb*(interval_end-dask_chunk)/
                                                    (endtime-starttime)))
        if b_islocalclient :
            client.close()

        return 0   
    
    '''
    def _write_by_region_old_dask_execute(self,):
        if self.verbose :
            print("Start dask")
        if dasklist :
            dasklistchunks.append(dasklist)
        #dasklistbag=dbag.from_delayed(dasklist)
        #write_statuss=dasklistbag.compute()
        for dasklistchunk in tqdm(dasklistchunks):
            starttime=time.time()
            write_statuss=compute(*dasklistchunk)
            endtime=time.time()
            if any((x:=write_status) > 0 for write_status in list(write_statuss)):
                return x   
            if self.verbose:
                print("Stored data with {0}mb/s".format(target_mb*len(dasklistchunk)/
                                                        (endtime-starttime)))
   
    def _write_by_region_old_dask_gather(self,):  
        b_printed1=False
        dasklist=[]
        dasklistchunks=[]
        try:
            if self.verbose and not b_printed1 :
                print("Start creating dask work chunks")
                b_printed1 = True
            dasklist.append(#fire_and_forget(
                delayed(self._write_chunk_by_region_wrapper)(towrite, store, chunkdim,
                                                             all_chunks, chunksum, chunk_no, is_safe_chunk))
                           #)
            #dask fills memory 5times the comparison number (5000 -> 25000)
            if len(dasklist) * target_mb > 5000:
                dasklistchunks.append(dasklist.copy())
                dasklist.clear()
            elif len(dasklist) > self.DASK_CHUNK_SIZE:
                dasklistchunks.append(dasklist.copy())
                dasklist.clear()
            b_isdask=True
        except :
            raise ValueError("Dask manually disabled.")
    '''

        
    def write_by_region(
        self, chunked_ds, already, store, startchunk, validity_check, chunkdim, varname, target_mb,
        is_safe_chunk, isnew=False
    ):
        if self.verbose :
            print("Reset cloud if overwrite and drop vars without chunkdim.")

        already, isnew = self._reset_already_if_overwrite(validity_check, already, chunked_ds, store, isnew, chunkdim, varname)
        
        try:
            already = self._drop_vars_without_chunkdim(already, chunkdim)
        except:
            print(
                "Could not drop vars without chunkdim.",
                " This is not an issue if you initialized the dataset in the cloud.",
            )
        chunked_ds = self._drop_vars_without_chunkdim(chunked_ds, chunkdim)  #
        
        all_chunks = chunked_ds.chunks[chunkdim]
        istartchunk = 0
        chunksum = 0
        
        if not isnew:
            if self.verbose :
                print("Get start chunk")
            #different and validity:
            status_equality, istartchunk = self._get_start_chunk(all_chunks, chunked_ds, chunksum, startchunk, chunkdim,
                        already, validity_check, varname)
            if status_equality > 0 :
                return status_equality
            
        if self.verbose :
            print(f"Start Looping over chunks beginning with {istartchunk}")

        if self.USE_DASK:
            self.write_by_region_dask(istartchunk,
                                      all_chunks,
                                      chunked_ds, 
                                      chunkdim,
                                      store,
                                      target_mb) 
        else:
            for chunk_no in tqdm(range(istartchunk, len(all_chunks))):
                chunksum += all_chunks[chunk_no]
                towrite = self._sel_range_for_chunk(
                    chunked_ds, chunksum - all_chunks[chunk_no], chunksum, chunkdim
                ) 
                if not isnew:
                    if chunk_no != istartchunk:
                        status_equality = self._check_for_equality(towrite, already, chunked_ds, all_chunks,
                                                               chunksum, chunkdim, 
                                                               validity_check, chunk_no, varname)
                        if status_equality < 0:
                            continue
                        elif status_equality > 0:
                            return status_equality
                towrite=towrite.drop_vars("time")
#                if self.USE_DASK:
#                    self._write_by_region_old_dask_gather()
#                except Exception as e:
#                    if self.verbose and not b_printed2 :
#                        print(f"Could not use dask because of {e}")
#                        b_printed2=True
                starttime=time.time()
                write_status = self._write_chunk_by_region_wrapper(towrite, store, chunkdim,
                                       all_chunks, chunksum, chunk_no, is_safe_chunk)
                endtime=time.time()
                if write_status > 0:
                    return write_status
                if self.verbose:
                    print("Stored data with {0}mb/s without dask".format(target_mb/(endtime-starttime)))
#            if self.USE_DASK:
#                self._write_by_region_old_dask_execut()
        return 0
    def write_directly(self, dset=None, store=None):
        dset.to_zarr(store=store, mode="w", consolidated=True)

    def write_with_validation_and_retries(
        self,
        ds,
        varname,
        store,
        chunkdim,
        target_mb,
        startchunk,
        validity_check,
        maxretries,
        trusted
    ):
        chunked_ds, is_safe_chunk = self._rechunk(ds, varname, chunkdim, target_mb)
        #
        retries = 0
        success = -1
        if startchunk != 0:
            success = startchunk
        
        if validity_check:
            trusted=True
        already, b_isnew = self._open_or_initialize_swift_dset(store, chunked_ds, chunkdim)
        if not validity_check and not b_isnew:
            if ( chunked_ds[varname].nbytes / (1024*1024*1024) ) < 4:
                if already.identical(chunked_ds):
                    if self.verbose:
                        print("The dataset is already completed in the cloud.")
                    return 0
            else :
                if self.verbose:
                    print("Could not compare datasets because size of array is {0}GB"
                      "and therefore larger than 4GB".format(
                          chunked_ds[varname].nbytes / (1024*1024*1024)))
        if b_isnew and startchunk != 0 :
            raise ValueError("Cannot start at startchunk {0} because the zarr dataset is new.".format(
                startchunk))

        while success != 0 and retries < maxretries:
            success = self.write_by_region(
                chunked_ds, already, store, success, validity_check, chunkdim, varname,
                target_mb, is_safe_chunk, isnew=b_isnew
            )
            retries += 1
            if self.verbose and success != 0:
                print("Write by region failed. Now retry number {}.".format(retries))
        if success != 0:
            raise RuntimeError(
                "Max retries {0} all failed at chunk no {1}".format(maxretries, success)
            )
        if not trusted:
            if self.verbose:
                print("Start validation of write process")
            if (
                self.write_by_region(chunked_ds, already, store, startchunk, True, chunkdim, varname,
                                     target_mb, is_safe_chunk)
                != 0
            ):
                raise RuntimeError("Validiation failed.")

    def write_to_swift(
        self,
        chunkdim="time",
        target_mb=1000,
        startchunk=0,
        validity_check=False,
        maxretries=3,
        trusted=True
    ):
        pargs=locals()
        writestore=self.store
        if not writestore.prefix :
            raise ValueError("First open a target store with open_store()")
        if self.fsspec_store :
            writestore = self.fsspec_store
            
        try:
            self.provenance.gen_output(pargs)
            self.provenance.write_json(self.store)
            self.provenance.write_png(self.store)
            self.mf_dset.attrs.setdefault("hasProvenance", "")
            temp=self.store.prefix
            self.store.prefix=""
            self.mf_dset.attrs["hasProvenance"]+=" "+'/'.join([self.store.url,
                                                               "provenance",
                                                               '_'.join(["provenance",
                                                                        temp,
                                                                        self.provenance.tracking_id+".json"])
                                                              ])
            self.store.prefix=temp
        except Exception as e:
            print(e)
            print("Could not create full provenance")
            pass

        self.chunkdim=chunkdim
        self.target_mb=target_mb
        timevars = [
            var
            for var in self.mf_dset.variables
            if chunkdim in self.mf_dset[var].coords
        ]
        if len(timevars) > 0:
            self.write_with_validation_and_retries(
                self.mf_dset,
                self.varname,
                writestore,
                chunkdim,
                target_mb,
                startchunk,
                validity_check,
                maxretries,
                trusted
            )
        else:
            self.write_directly(self.mf_dset, writestore)
        self.mf_dset.close()
        
        try:
            self.write_index_file()
        except Exception as e:
            print(e)
            print("Could not write index file")
            pass

           
        returnstore = SwiftStore(
            self.store.container,
            prefix=self.store.prefix,
            storage_options=self.store.storage_options,
        )
        if self.fsspec_store :
            returnstore = copy.deepcopy(self.fsspec_store)
            
        return returnstore
    
    def write_index_file(self, pattern=None, contact=None):
        catalog.write_index_file(self.store, pattern, contact)

    def write_catalog(self,
                  catalogname="catalog.json",
                  pattern=None,
                  delim=".",
                  columns=[],
                  mode="a"
                 ):
        catalog.write_catalog(self.store,
                  catalogname="catalog.json",
                  pattern=None,
                  delim=".",
                  columns=[],
                  mode="a",
                  verbose=self.verbose
                 )