#!/usr/bin/env python
# coding: utf-8

import requests
import os
import os.path as osp
import getpass
from zarrswift import SwiftStore
from datetime import datetime
from pathlib import Path
import fsspec

def get_fsspec_store(os_url, os_token):
    try:
        fsspec_store=fsspec.get_mapper(os_url)
        fsspec_store[".test"]=b"test"
        del fsspec_store[".test"]
        return fsspec_store
    except Exception as e:
        print(e)
        print("Could not use fsspec implementation")
        return None

def get_fsspec_url(os_url, os_container):
    if os_url.startswith("https"):
        os_url=os_url.replace("https", "swift")
    elif os_url.startswith("http"):
        os_url=os_url.replace("http", "swift")
    os_url=os_url.replace("v1/","")
    os_url='/'.join([os_url, os_container])
    return os_url


def _check_connection(account, user, password=None):

    SWIFT_BASE = "https://swift.dkrz.de/"
    SWIFT_VERS = "v1.0"

    headers = {"X-Auth-User": f"{account}:{user}"}
    if password:
        headers["X-Auth-Key"] = password
    resp = requests.get(SWIFT_BASE + f"auth/{SWIFT_VERS}", headers=headers)
    try:
        resp.raise_for_status()
    except requests.RequestException:
        raise requests.RequestException(f"Connection to {SWIFT_BASE} failed")
    return resp


def _write_token(token, storage_url, expires, token_file="~/.swiftenv"):
    """Write credentials to a token file for permanent usage.

    Parameters:
    ==========
        token: str
            New swift login token
        storage_url: str
            url associated with a swift container and account
        expires: str
            expiry date of the token
        token_file : str, pathlib.Path, default: ~/.swiftenv
            filename where credentials are stored.
    """

    env = {
        "OS_AUTH_TOKEN": token,
        "OS_STORAGE_URL": storage_url,
        "OS_AUTH_URL": '" "',
        "OS_USERNAME": '" "',
        "OS_PASSWORD": '" "',
    }
    with open(osp.expanduser(token_file), "w") as f:
        f.write(f"#token expires on: {expires}\n")
        for key, value in env.items():
            f.write(f"setenv {key} {value}\n")
    os.chmod(osp.expanduser(token_file), stat.S_IREAD | stat.S_IWRITE)


def _check_token_for_user_and_account(account, username):
    try:
        with Path("~/.swiftenv_useracc").expanduser().open("r") as f:
            user = f.readline().strip().strip("\n").lower() == username.lower()
            acc = f.readline().strip().strip("\n").lower() == account.lower()
    except FileNotFoundError:
        user, acc = False, False
    with Path("~/.swiftenv_useracc").expanduser().open("w") as f:
        f.write(username + "\n")
        f.write(account + "\n")
    return all((user, acc))


def _get_envtoken(token_file="~/.swiftenv", **kwargs):
    """Read current swift credentials."""

    env = {}
    with open(os.path.expanduser(token_file)) as f:
        for nn, line in enumerate(f.readlines()):
            if nn == 0 and line.startswith("#"):
                date_str = line.strip("\n").partition(":")[-1].strip()
                expires = datetime.strptime(date_str, "%a %d. %b %H:%M:%S %Z %Y")
            if line.startswith("setenv"):
                content = line.strip("\n").partition(" ")[-1].split(" ")
                value = " ".join(content[1:]).strip('"').strip("'")
                env[content[0]] = value
    return env, expires


def create_token(account, user, **kwargs):
    """Create a new swift token for a given account."""

    user = user or getpass.getuser()
    password = getpass.getpass(
        (f"Creating new swift-token" f"\nGive password for {user} on {account}: ")
    )
    resp = _check_connection(account, user, password)
    tz = time.tzname[-1]
    expires = token = storage_url = None
    token = resp.headers["x-auth-token"]
    storage_url = resp.headers["x-storage-url"]
    expires_in = int(resp.headers["x-auth-token-expires"])
    expires_at = datetime.fromtimestamp(time.time() + expires_in)
    expires = expires_at.strftime("%a %d. %b %H:%M:%S {tz} %Y".format(tz=tz))
    _write_token(token, storage_url, expires, **kwargs)


def get_token(account, username=None, **kwargs):
    """Check if a swift token for a given account is still valid.

    This method checks if a token for a swift container can be used
    to log on to swift and if the token is valid for at least one day.
    Otherwise a new token will be created.

    Parameters:
    ===========
        account : str
            Account name of the associated swift container
        username : str (default None)
            User name for the swift token, this system will get the
            username the runs this program if None is given (default)
    Returns:
    ========
        dict: Dictionary containing the swift credentials
    """

    try:
        env, expires = _get_envtoken(**kwargs)
    except FileNotFoundError:
        env, expires = {}, datetime.now()
    valid = _check_token_for_user_and_account(account, username or getpass.getuser())
    if (
        (expires - datetime.now()).total_seconds() / 60 ** 2 > 24
        and env.get("OS_AUTH_TOKEN", "")
        and env.get("OS_STORAGE_URL")
        and valid
    ):
        return env
    create_token(account, username or None)
    return _get_envtoken(**kwargs)


def toggle_public(store):
    "toggle container public read_acl settings"
    acl = store.conn.head_container(store.container).get("x-container-read", "")
    if ".r:*" in acl:
        acl = acl.replace(".r:*", "")
    else:
        acl = ",".join([acl, ".r:*,.rlistings"])
    acl = ",".join(sorted(filter(None, set(acl.split(",")))))
    store.conn.post_container(store.container, headers={"X-Container-Read": acl})