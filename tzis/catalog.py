import io
import csv
from zarrswift import SwiftStore
from datetime import datetime
import requests
import json

def write_index_file(store, pattern=None, contact=None):
    prefix=store.prefix
    store.prefix=""
    index_name="INDEX.csv"
    header=["contact",
            "prefix",
            "size",
            "first_modification",
            "last_modification",
            "url"]

    output = io.StringIO()
    writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)

    writer.writerow(header)

    all_zarr_datasets = store.listdir()
    catalog = next((zd for zd in all_zarr_datasets if "catalog" in zd), None)
    if catalog:
        all_zarr_datasets.remove(catalog)
    if index_name in all_zarr_datasets:
        del store[index_name]
        all_zarr_datasets.remove(index_name)
    if pattern:
        all_zarr_datasets = [zd for zd in all_zarr_datasets
                            if pattern in zd]
    blacklist=["provenance"]
    for outsort in blacklist:
        all_zarr_datasets = [zd for zd in all_zarr_datasets
                            if outsort not in zd]


    for zd in all_zarr_datasets:
        contents = store._list_container(
            zd, strip_prefix=True, treat_path_as_dir=False
        )
        modiflist=[]
        sumbytes=0
        for entry in contents:
            sumbytes+=entry["bytes"]
            modiflist.append(datetime.strptime(
                entry.get("last_modified", 0).split('.')[0],
                "%Y-%m-%dT%H:%M:%S"
            )
                            )
        writer.writerow([contact,
                         zd,
                         sumbytes,
                         min(modiflist),
                         max(modiflist),
                         '/'.join([store.url,
                                   zd])])

    #all_zarr_datasets_bytes = u'\n'.join(all_zarr_datasets).encode('utf8').strip()
    store[index_name]=bytes(output.getvalue(),"utf8")
    store.prefix=prefix
        
def write_catalog(store,
                  catalogname="catalog.json",
                  pattern=None,
                  delim=".",
                  columns=[],
                  mode="a",
                  verbose=True
                 ):
    header={#"varname":varname,
            #"chunkdim":chunkdim,
            #"target_mb":target_mb,
            "prefix":None,
            "url":None}

    prefix=store.prefix
    store.prefix=""
    returnurl='/'.join([store.url,catalogname])

    if not len(delim) == 1:
        raise ValueError("Delim must have lenght 1 but {0} has length {1}".format(
            delim, len(delim))
                        )

    all_zarr_datasets = store.listdir()

    b_append=True
    if not catalogname in all_zarr_datasets:
        b_append=False
    else :
        all_zarr_datasets.remove(catalogname)
    if mode == "w" :
        b_append=False

    if b_append and prefix not in all_zarr_datasets:
        if verbose:
            print("This prefix {0} has not been written yet to cloud".format(prefix))
        return returnurl 

    if b_append and columns:
        if verbose:
            print("Use columns from existing catalog. Given columns will be ignored.")
        columns=[]          

    if pattern:
        all_zarr_datasets = [zd for zd in all_zarr_datasets
                            if pattern in zd]
    blacklist=["provenance"]
    for outsort in blacklist:
        all_zarr_datasets = [zd for zd in all_zarr_datasets
                            if outsort not in zd]

    index = next((zd for zd in all_zarr_datasets if "INDEX." in zd), None)
    if index:
        all_zarr_datasets.remove(index)

    if not all_zarr_datasets:
        print("No dataset found to be written to catalog")
        return 1

    b_usedelim=True

    if b_append:
        catalog=requests.get('/'.join([store.url,catalogname])).json()
        if any(prefix == entry["prefix"] for entry in catalog["catalog_dict"]):
            if verbose:
                print("Dataset {0} already in catalog".format(prefix))
            return returnurl
        for coldict in catalog["attributes"]:
            columns.append(coldict.keys()[0])

        if len(prefix.split(delim)) + len(header) != len(columns) :
            if verbose:
                print("Length of registered columns {0} unequals length of legnth of splitted dset {1}".format(
                    len(columns), len(prefix.split(delim))))

        #if list(catalog["catalog_dict"][0].keys()) != list(header.keys()) + columns :
        #    raise ValueError("Keys in catalog_dict {0} unequals written given columns {1}".format(
        #        list(catalog["catalog_dict"][0].keys()), list(header.keys()) + columns))
        del store[catalogname]
    else :
        max_col_length=max([len(zd.split(delim)) for zd in all_zarr_datasets])
        min_col_length=min([len(zd.split(delim)) for zd in all_zarr_datasets])

        if max_col_length == 1 and verbose:
            print("Cannot parse items to columns with delim {0}".format(delim))
            if columns:
                print("columns will be ignored.")
                b_usedelim=False
        if columns :
            if min_col_length < len(columns) and verbose:
                print("Cannot parse all dataset names to all columns with delim {0}".format(delim))
            if max_col_length > len(columns) and verbose:
                print("Too less column values specified in columns")

        template_url="https://swift.dkrz.de/v1/dkrz_a44962e3ba914c309a7421573a6949a6/intake-esm/dkrz_tzis_template.json"
        catalog=requests.get(template_url).json()
        catalog["catalog_dict"]=[]        

    if b_append :
        dictentry=header.copy()
        dictentry["prefix"]=prefix
        dictentry["url"]='/'.join([store.url,prefix])
        if b_usedelim:
            for key,item in zip(columns, prefix.split(delim)):
                dictentry[key]=item
        catalog["catalog_dict"].append(dictentry)
    else :
        if b_usedelim:
            if columns :
                for item in columns:
                    catalog["attributes"].append({"column_name": item})
            else :
                if verbose:
                    print("You did not specify column names. We count through them.")
        for zd in all_zarr_datasets:
            dictentry=header.copy()
            dictentry["prefix"]=zd
            dictentry["url"]='/'.join([store.url,zd])
            if b_usedelim:
                for key,item in zip(columns, zd.split(delim)):
                    dictentry[key]=item
            catalog["catalog_dict"].append(dictentry)
    store[catalogname]=json.dumps(catalog, indent=4).encode('utf-8')
    store.prefix=prefix
    return returnurl