# From Roocs
#
import os
import uuid
import json
import io
from datetime import datetime
from zarrswift import SwiftStore

from prov.identifier import Namespace
import prov.model as prov
from prov.dot import prov_to_dot

# prov namespace
PROV_ORGANISATION = prov.PROV["Organization"]
PROV_SOFTWARE_AGENT = prov.PROV["SoftwareAgent"]

# provone namespace
PROVONE = Namespace(
    "provone", uri="http://purl.dataone.org/provone/2015/01/15/ontology#"
)
PROVONE_DATA = PROVONE["Data"]

# dcterms namespace
DCTERMS = Namespace("dct", uri="http://purl.org/dc/terms/")
DCTERMS_SOURCE = DCTERMS["source"]

# tzis namespace
TZIS = Namespace("tzis", uri="urn:tzis:")

class Provenance(object):
    def __init__(self):
        from .tzis import __version__ as tzis_version
        self._identifier = None
        self.tzis_version = tzis_version
        
        # Create an empyty prov document
        self.doc = prov.ProvDocument()

        self._identifier = uuid.uuid4()
        self.tracking_id = str(uuid.uuid4())
        # Declaring namespaces for various prefixes
        self.doc.add_namespace(TZIS)
        self.doc.add_namespace(PROVONE)
        self.doc.add_namespace(DCTERMS)
        
        tzis_write = self.doc.activity(TZIS["write_to_swift"])

        # Define entities
        dkrz_agent = self.doc.agent(
            TZIS["DKRZ"],
            {
                prov.PROV_TYPE: PROV_ORGANISATION,
                prov.PROV_LABEL: "German Climate Computing Center",
                #DCTERMS_SOURCE: "https://cds.climate.copernicus.eu",
            },
        )
        sw_tzis = self.doc.agent(
            TZIS[f"tzis_v{self.tzis_version}"],
            {
                prov.PROV_TYPE: PROV_SOFTWARE_AGENT,
                prov.PROV_LABEL: "tzis",
                DCTERMS_SOURCE: f"https://gitlab.dkrz.de/data-infrastructure-services/tzis",
            },
        )
                # Create relations between the two entities
        self.doc.wasAssociatedWith(tzis_write, sw_tzis)
        self.doc.wasAttributedTo(sw_tzis, dkrz_agent)


    @property
    def identifier(self):
        return self._identifier

    def gen_output(self, pargs):
        output_data = self.doc.entity(
            TZIS[self.tracking_id],
            {
                prov.PROV_TYPE: PROVONE_DATA,
                prov.PROV_LABEL: "output_tracking_id",
            },
        )
                
        self.doc.wasGeneratedBy(TZIS[self.tracking_id],
                                TZIS["write_to_swift"])
        activity = self.doc.activity(TZIS["write_to_swift"])
        attributes={}
        default_atts=["chunkdim", "startchunk", "target_mb"]
        for key in default_atts:
            attributes[TZIS[key]]=pargs[key]
        activity.add_attributes(attributes)

        
    def gen_input(self, input_type, input_ids):
        for tid in input_ids:
            entity = self.doc.entity(TZIS[tid],
                                     {
                                         prov.PROV_TYPE: PROVONE_DATA,
                                         prov.PROV_LABEL: input_type,
                                     }
                                    )
            self.doc.wasDerivedFrom(TZIS[self.tracking_id],
                                    TZIS[tid])
            self.doc.used(TZIS["write_to_swift"],
                          TZIS[tid])
            
             
    def write_json(self, store):
        output = io.StringIO()
        self.doc.serialize(destination=output)
        #
        dset=store.prefix
        store.prefix="provenance"        
        store[f"provenance_{dset}_{self.tracking_id}.json"]=bytes(output.getvalue(),"utf8")
        store.prefix=dset

    def write_png(self, store):
        figure = prov_to_dot(self.doc)        
        dset=store.prefix
        store.prefix="provenance"
        store[f"provenance_{dset}_{self.tracking_id}.png"]=figure.create_png()
        store.prefix=dset

    def get_provn(self):
        return self.doc.get_provn()

    def dump_json(self):
        return self.doc.serialize(indent=2)

    def json(self):
        return json.loads(self.dump_json())