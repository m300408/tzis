from setuptools import setup, find_packages

setup(
    name='tzis',
    version='0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='Convert to Zarr in Swift Object Storage',
    long_description=open('README.md').read(),
    install_requires=['zarr', 'xarray', 'dask', 'netcdf4', 'tqdm', 'prov', 'pydot','fsspec',
                      'pandas<1.4.0',   #https://github.com/pydata/xarray/issues/6226
                      'zarr-swiftstore @ git+https://github.com/siligam/zarr-swiftstore.git',
                      'swiftspec @ git+https://github.com/fsspec/swiftspec',
		      'lxml'],
    url='???',
    author='Fabian Wachsmann, Marco Kulüke',
    author_email='wachsmann@dkrz.de'
)
