#!/bin/sh
conda install python=3.10 cdo python-cdo -c conda-forge -y
cd files
for file in $(ls -1 ./ ); do
  sha=$(cat $file | grep sha256 | cut -d ':' -f 2);
  wget -q https://swift.dkrz.de/v1/dkrz_0b2a0dcc-1430-4a8a-9f25-a6cb8924d92b/tzis-tests_input-files/${sha} ;
  mv $sha $file ;
done
ls ./
cd ../
jupyter nbconvert --to python test_functions.ipynb
jupyter nbconvert --to python test_validations.ipynb
#python test_functions.py >test-results.txt 2>&1 
python test_validations.py >>test-results.txt 2>&1

